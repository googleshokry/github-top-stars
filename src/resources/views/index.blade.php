@extends('layout.master')
@section('content')
    <h1 style="text-align: center;padding-top: 50px">Github Top Stars</h1>
    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        @if(isset($responses['message']))
            <div class="row">
                <h1>{{$responses['message']}}</h1>
            </div>
        @else
            <div class="row">

                {!! Form::open(['method' => 'get', 'route' => ['responses.index',request()->input()]]) !!}
                <div class="col-lg-6 portlets">
                    <select name="numberOfItems">
                        <option value="10" {{request()->numberOfItems==10?'selected':''}}>10</option>
                        <option value="50" {{request()->numberOfItems==50?'selected':''}}>50</option>
                        <option value="100" {{request()->numberOfItems==100?'selected':''}}>100</option>
                    </select>
                </div>
                <div class="col-lg-6 portlets" style="text-align: right;float: right;">
                    <label for="Created_at"> Created At</label>
                    <input type="date" name="date"
                           value="{{ request()->date??Carbon\Carbon::yesterday()->format('Y-m-d') }}">
                    {!! Form::submit('search', ['class' => 'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <div class="row">
                <div class="col-lg-12 portlets">
                    <div class="panel">

                        <div class="panel-content pagination2 table-responsive">
                            <table class="table table-hover ">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Full Name</th>
                                    <th>URL</th>
                                    <th>Created AT</th>
                                    <th>Updated AT</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($responses as $res)
                                    <tr>
                                        <td>{{$res['id']}}</td>
                                        <td>{{$res['full_name']}}</td>
                                        <td><a href="{{ $res['html_url'] }}"> {{$res['html_url']}}</a></td>
                                        <td>{{$res['created_at']}}</td>
                                        <td>{{$res['updated_at']}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            {{$responses->appends(request()->input())->links()}}
        @endif

    </div>
    <!-- END PAGE CONTENT -->
@endsection
