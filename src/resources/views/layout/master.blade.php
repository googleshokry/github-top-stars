<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="themes-lab">
    <link rel="shortcut icon" href="../assets/global/images/favicon.png" type="image/png">
    <!-- BEGIN PAGE STYLE -->
    <link href="../assets/global/plugins/datatables/dataTables.min.css" rel="stylesheet">
    <!-- END PAGE STYLE -->
    <link href="../assets/global/css/style.css" rel="stylesheet">
    <link href="../assets/global/css/theme.css" rel="stylesheet">
    <link href="../assets/global/css/ui.css" rel="stylesheet">
    <link href="../assets/admin/layout1/css/layout.css" rel="stylesheet">
    <script src="../assets/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <style>
        .main-content {
            margin-left: 0 !important;
        }
        .hidden{
            display: block!important;
        }
        span.relative.z-0.inline-flex.shadow-sm.rounded-md {
            display: none;
        }
    </style>
</head>
<body class="fixed-topbar fixed-sidebar theme-sdtl color-default">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<section>

    <div class="main-content">
@yield('content')
    </div>
    <!-- END MAIN CONTENT -->

</section>

<!-- BEGIN PRELOADER -->
<div class="loader-overlay">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<!-- END PRELOADER -->
<script src="../assets/global/plugins/jquery/jquery-3.1.0.min.js"></script>
<script src="../assets/global/plugins/jquery/jquery-migrate-3.0.0.min.js"></script>
<script src="../assets/global/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/global/plugins/appear/jquery.appear.js"></script>

<script src="../assets/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- Animated Progress Bar -->
<script src="../assets/global/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
<script src="../assets/global/js/application.js"></script> <!-- Main Application Script -->
<script src="../assets/global/js/plugins.js"></script> <!-- Main Plugin Initialization Script -->

<!-- BEGIN PAGE SCRIPTS -->
<script src="../assets/global/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- Tables Filtering, Sorting & Editing -->
<script src="../assets/global/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="../assets/global/js/pages/table_dynamic.js"></script>
<!-- END PAGE SCRIPTS -->
<script src="../assets/admin/layout1/js/layout.js"></script>
</body>
</html>
