<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\Table;

class repositories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "repositories:all {date} {numberOfItems?}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Github Top Stars';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = $this->argument('date');
        $numberOfItems = $this->argument('numberOfItems');
        $response = \App\Services\Repositories::search(['date' => $date, 'numberOfItems' => $numberOfItems]);
        // Create a new Table instance.
        $table = new Table($this->output);
        $table->setHeaders(['ID', 'Full Name','URL' ,'Created_AT','Updated_AT']);
        $table->setRows($response['items']);
        $table->render();
    }


}
