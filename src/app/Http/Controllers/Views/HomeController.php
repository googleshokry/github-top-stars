<?php

namespace App\Http\Controllers\Views;

use App\Services\Repositories;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $responses = Repositories::search($request->query());

        if (!isset($responses['message'])) {
            $newcollection = Collect($responses);
            $data = $this->paginate($newcollection['items'], $newcollection['total_count'], request()->numberOfItems ?? 1, request()->page ?? 1);
        }
        return view('index', ['responses' => $data ?? $responses]);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginate($items, $total, $perPage = 5, $page = null, $options = [])
    {
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items, $total, $perPage, $page, $options);
    }
}
