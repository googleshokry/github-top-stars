<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class APIController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var bool
     */
    private $result = false;
    /**
     * @var array
     */
    private $content = [];
    /**
     * @var int
     */
    private $errorType = 0;

    private $log = true;

    /**
     * @param bool|false $result
     * @return $this
     */
    public function setResult($result = false)
    {
        if (!$result)
            abort(($this->getErrorType() == 0) ? 200 : $this->getErrorType(), !empty($this->getContent()) ? $this->getContent()['data'] : (!empty($this->getContent()['message']) ? $this->getContent()['message'] : "not know error"));
        $this->result = $result;
        return $this;
    }

    /**
     * @param $data
     * @return static
     */
    public function setDataKey($data)
    {
        return $this->setContent(['data' => $data]);
    }

    /**
     * @param $data
     * @return static
     */
    public function setErrorKey($data)
    {
        $this->content['errors'] = $data;
        return $this;
    }

    public function setLog($data)
    {
        $this->log = $data;
        return $this;
    }

    /**
     * @param $data
     * @return static
     */
    public function setMessageKey($data)
    {
        $this->content['message'] = $data;
        return $this;
    }

    /**
     * @param array $message
     * @return $this
     */
    public function setContent($message)
    {
        $this->content['data'] = $message;
        return $this;
    }

    /**
     * @param int $data
     * @return $this
     */
    public function setErrorCode($data = 0)
    {
        $this->errorType = $data;
        return $this;
    }

    /**
     * @return bool
     */
    private function getResult()
    {
        return $this->result;
    }

    /**
     * @return array
     */
    private function getContent()
    {
        $this->setRefreshToken();
        return $this->content;
    }

    /**
     * @return bool
     */
    private function getLog()
    {
        return $this->log;
    }

    /**
     * @return array
     */
    private function Content()
    {
        return $this->content;
    }

    /**
     * @return bool
     */
    private function Log()
    {
        return $this->log;
    }

    /**
     * @return int
     */
    private function getErrorType()
    {
        return $this->errorType;
    }

    public function setRefreshToken()
    {
        $str = '****';
        $token = request()->get('token') ?? 'kk';
        if (str_contains($token, $str) !== false) {
            $this->content['token'] = str_replace("****", '', $token);
            return $this;
        }
        if ($this->errorType !== 0) {
            return $this;
        }
        $this->content['token'] = null;
        return $this;
    }

    /**
     *
     */
    public function send()
    {
        $rs = [
            'result' => $this->getResult(),
            'content' => (object)$this->getContent(),
            'code' => ($this->getErrorType() == 0) ? 200 : $this->getErrorType()
        ];
        return response()->json($rs, ($this->getErrorType() == 0) ? 200 : $this->getErrorType(), ['content-type' => 'application/json']);
    }

    public function sendRequest()
    {
        $rs = [
            'result' => $this->getResult(),
            'content' => (object)$this->getContent(),
            'code' => ($this->getErrorType() == 0) ? 200 : $this->getErrorType()
        ];

        return response()->json($rs, ($this->getErrorType() == 0) ? 200 : $this->getErrorType(), ['content-type' => 'application/json']);
    }

}
