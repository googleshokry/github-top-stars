<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\APIController;
use App\Services\Repositories;
use Illuminate\Http\Request;

class HomeController extends APIController
{
    public function index(Request $request)
    {
        $response = Repositories::search($request->query());
        return $this->setLog(false)->setContent($response)->setResult(true)->send();
    }

}
