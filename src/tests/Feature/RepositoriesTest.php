<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class RepositoriesTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_example()
    {
        // view
        $response = $this->get('/');
        $response->assertSee('Github Top Stars');
        $response->assertStatus(200);

    }

    public function testJson()
    {
        // json
        $response = $this->get('/v1/api/repositories');
        $response->assertHeader('content-type', 'application/json')
            ->assertStatus(200)
            ->assertJsonStructure([
                'content' => [
                    'data' => [
                        'items' => [
                            '*' => [
                                'id',
                                'full_name',
                                'created_at',
                                'updated_at',
                                'html_url'
                            ]
                        ]
                    ]
                ]
            ])
            ->assertJsonCount(10, 'content.data.items')
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );
        // json by date
        $response = $this->get('/v1/api/repositories?numberOfItems=50&date=2021-05-11');
        $response->assertHeader('content-type', 'application/json')
            ->assertStatus(200)
            ->assertJsonPath('content.data.items.0.id', 366346891)
            ->assertJsonStructure([
                'content' => [
                    'data' => [
                        'items' => [
                            '*' => [
                                'id',
                                'full_name',
                                'created_at',
                                'updated_at',
                                'html_url'
                            ]
                        ]
                    ]
                ]
            ])
            ->assertJsonCount(50, 'content.data.items')
            ->assertJson(fn(AssertableJson $json) => $json->hasAll(['result', 'code', 'content'])
            );
    }
}
