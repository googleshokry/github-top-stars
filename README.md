## Usage

To get started, make sure you have [Docker installed](https://docs.docker.com/docker-for-mac/install/) on your system, and then clone this repository.

Next, navigate in your terminal to the directory you cloned this, and spin up the containers for the web server by running `docker-compose up -d --build site`.

After that completes, run this code

- `docker-compose run --rm composer update`

Note:- 
The following are built for our web server, with their exposed ports detailed:

- **nginx** - `:80`
- **php** - `:9000`


You can use By

View by 

``
http://localhost
``

API By

``
http://localhost/v1/api/repositories?date=2019-01-10&sortOfNumber=3
``

CLI By

``
php artisan repositories:all 2021-04-04 5
``

for Testing run 

``
composer test
``

or

``./vendor/bin/phpunit
``
